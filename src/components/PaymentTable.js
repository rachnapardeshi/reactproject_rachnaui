import React from "react";
import {Component} from "react";
import axios from 'axios';
import logo from "../payment.jpg";
import Moment from "moment";

class PaymentTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paymentList:[],
            count:0
        }
    }

    render() {
        return(
            <div>
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>
                        AllState Payment System Application
                    </p>
                </header>
            <div className="container">
                <h1 align="center"> Payment List {this.props.name}</h1>
                <table class="table table-striped" valign="center" width="80%">
                  <thead class="danger">
                    <th>
                        Payment Id
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Type
                    </th>
                    <th>
                        Amount
                    </th>
                    <th>
                        Customer Id
                    </th>
                    </thead>
                   <tbody>
                    {this.state.paymentList.map(d=> (<tr class="success">
                        <td>{d.id}</td>
                        <td>{d.date}</td>
                        <td>{d.type}</td>
                        <td>{d.amount}</td>
                        <td>{d.custId}</td>
                    </tr>))}
                   </tbody>
                </table>
            </div>
            </div>
        )
    }

    componentDidMount() {
        //this.setState({name:"deirdre"});
        axios
            .get("http://localhost:8080/api/payment/all")
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response)
                const newPayments = response.data.map(c => {
                    return {
                        id: c.id,
                        date: Moment(c.paymentDate).format('DD-MMM-YYYY'),
                        type: c.type,
                        amount: c.amount,
                        custId: c.custId
                    };
                });
                this.setState({paymentList: newPayments})
                console.log(newPayments)
            })

            .catch(function (error) {
                console.log(error);
            });
    }

}

export default PaymentTable;