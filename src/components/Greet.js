import React from "react";

function Greet(props){
    return(
        <div className="container">
            <h2 align="center"> Welcome {props.name}</h2>
        </div>
    )
}

export default Greet;