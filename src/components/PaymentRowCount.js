import React from "react";
import {Component} from "react";
import axios from 'axios';

class PaymentRowCount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status:""
        }
    }

    render() {
        return(
            <div>
                <h3> {this.state.status} </h3>
            </div>
        )
    }

    componentDidMount() {
        axios
            .get("http://localhost:8080/api/payment/rowcount")
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response)
                const newStatus = response.data;
                this.setState({status: newStatus})
                console.log(newStatus)
            })

            .catch(function (error) {
                console.log(error);
            });
    }

}

export default PaymentRowCount;