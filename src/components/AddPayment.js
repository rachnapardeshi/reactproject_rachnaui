import React, {Component} from 'react';
import axios from "axios";

export default class AddPayment extends Component {
    constructor(props) {
        super(props);
        this.onChangePaymentId = this.onChangePaymentId.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeCustomerId = this.onChangeCustomerId.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state = {
            id: "",
            type: "",
            amount:"",
            custId: ""
        };
    }

    onChangePaymentId(e) {
        this.setState({
            paymentId: e.target.value
        });
    }
    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }
    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }
    onChangeCustomerId(e) {
        this.setState({
            custId: e.target.value
        });
    }

    savePayment() {
        var data = {
            id: this.state.paymentId,
            type: this.state.type,
            amount: this.state.amount,
            custId: this.state.custId}


        axios.post('http://localhost:8080/api/payment/save', data)
            .then(response => this.setState({ submitted: response.data.id }))
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        return (<form>
            <div className="container">
                <div className="panel panel-info">
                    <div className="panel-heading">
                        Add Payment
                    </div>
                    <div className="panel-body">
                        <div className="form-group">
                <label  className=".control-label" htmlFor="txtname"> Payment Id </label>
                <input type="text" className="form-control" id="paymentId" aria-describedby="fnameHelp"
                       placeholder="Enter Payment Id " value={this.state.paymentId} onChange={this.onChangePaymentId}/>
            </div>
            <div className="form-group">
                <label className=".control-label" htmlFor="txtname"> Type </label>
                <input type="text" className="form-control" id="type" aria-describedby="fnameHelp"
                       placeholder="Enter Payment Type" value={this.state.type} onChange={this.onChangeType}/>
            </div>

            <div className="form-group">
                <label> Amount </label>
                <input type="text" className="form-control" id="amount" aria-describedby="fnameHelp"
                       placeholder="Enter Amount" value={this.state.amount} onChange={this.onChangeAmount}/>
            </div>

            <div className="form-group">
                <label>Customer Id</label>
                <input type="number" className="form-control" id="customerId" aria-describedby="emailid"
                       placeholder="Enter Customer Id" value={this.state.custId} onChange={this.onChangeCustomerId}/>
            </div>

            <div className="form-group">
                <input type="submit" className="btn btn-success"  onClick={this.savePayment}/>
            </div>
                    </div>
                </div>
            </div>

        </form>)
    }
}

