import React, {Component} from 'react';
import logo from "../payment.jpg";
import Moment from "moment";

const url = "http://localhost:8080/api/payment/findbytype/";
export default class FindByType extends Component {
    constructor(props) {
        super(props);
        this.onChangePaymentId = this.onChangePaymentId.bind(this);
        this.state = {
            id:'',
            amount:'',
            date:'',
            type:'',
            custId:'',
            paymentData:[]
        };
    }

    onChangePaymentId = (e) => {
        console.log(e.target.value)
        this.setState({
            type: e.target.value
        });

        const cityId = e.target.value
        fetch(`${url}${cityId}`, {
            method: "GET",
            dataType: "JSON",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            }
        })
            .then((resp) => {
                return resp.json()
            })
            .then((data) => {
                const newPayments = data.map(c => {
                    return {
                        id: c.id,
                        date: Moment(c.paymentDate).format('DD-MMM-YYYY'),
                        type: c.type,
                        amount: c.amount,
                        custId: c.custId
                    };
                });
                this.setState({paymentData: newPayments})
            })
            .catch((error) => {
                console.log(error, "catch the hoop")
            })
    }

    render() {
        console.log("inside render ----- ")
        return (
            <div>

                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>
                        AllState Payment System Application
                    </p>
                </header>
            <div className="container">
                <form>
                    <div className="container">
                        <div className="panel panel-info">
                            <div className="panel-heading">
                                Find By Type
                            </div>
                            <div className="panel-body">
                                <div className="form-group">
                                    <label  className=".control-label" htmlFor="txtname"> Payment Type </label>
                                    <input type="text" className="form-control" id="id" aria-describedby="fnameHelp"
                                           placeholder="Enter Payment Type " value={this.state.type} onChange={this.onChangePaymentId}/>
                                </div>
                                <div className="form-group">
                                    <button className="btn btn-success"  onClick={this.fetchData}> Submit </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div>
                    <table className="table table-striped" valign="center">
                        <thead className="success">
                        <th>
                            Payment Id
                        </th>
                        <th>
                            Date
                        </th>
                        <th>
                            Type
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Customer Id
                        </th>
                        </thead>
                        <tbody>
                        {this.state.paymentData.map(d=> (<tr class="danger">
                            <td>{d.id}</td>
                            <td>{d.date}</td>
                            <td>{d.type}</td>
                            <td>{d.amount}</td>
                            <td>{d.custId}</td>
                        </tr>))}
                        </tbody>
                    </table>
                </div>
            </div>
            </div>)
    }
}

