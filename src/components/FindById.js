import React, {Component} from 'react';
import logo from "../payment.jpg";
import Moment from "moment";

const url = "http://localhost:8080/api/payment/find/";
export default class FindById extends Component {
    constructor(props) {
        super(props);
        this.onChangePaymentId = this.onChangePaymentId.bind(this);
        this.state = {
            id:'',
            amount:'',
            date:'',
            type:'',
            custId:''
        };
    }

    onChangePaymentId = (e) => {
        console.log(e.target.value)
        this.setState({
            id: e.target.value
        });

        this.setState({ amount: ' ',
            date:' ',
            custId:' ',
            type:' '})

      const cityId = e.target.value
        fetch(`${url}${cityId}`, {
            method: "GET",
            dataType: "JSON",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            }
        })
            .then((resp) => {
                return resp.json()
            })
            .then((data) => {
                this.setState({ amount: data.amount,
                    id:data.id,
                    date: Moment(data.paymentDate).format('DD-MMM-YYYY'),
                    custId:data.custId,
                    type:data.type})
            })
            .catch((error) => {
                console.log(error, "catch the hoop")
            })

    }

    fetchData = () => {
            console.log(this.state.id)
    }

    render() {
        console.log("inside render ----- ")
        return (
            <div>

                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>
                        AllState Payment System Application
                    </p>
                </header>
            <div className="container">
            <form>
            <div className="container">
                <div className="panel panel-info">
                    <div className="panel-heading">
                        Find By Id
                    </div>
                    <div className="panel-body">
                        <div className="form-group">
                            <label  className=".control-label" htmlFor="txtname"> Payment Id </label>
                            <input type="text" className="form-control" id="id" aria-describedby="fnameHelp"
                                   placeholder="Enter Payment Id " value={this.state.id} onChange={this.onChangePaymentId}/>
                        </div>
                        <div className="form-group">
                            <button className="btn btn-success"  onClick={this.fetchData}> Submit </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div>
            <table className="table table-striped" valign="center">
                <thead className="danger">
                <th>
                    Payment Id
                </th>
                <th>
                    Date
                </th>
                <th>
                    Type
                </th>
                <th>
                    Amount
                </th>
                <th>
                    Customer Id
                </th>
                </thead>
                <tbody>
                <tr>
                    <td>{this.state.id}</td>
                    <td>{this.state.date}</td>
                    <td>{this.state.type}</td>
                    <td>{this.state.amount}</td>
                    <td>{this.state.custId}</td>
                </tr>
                </tbody>
            </table>
        </div>
      </div>
            </div>)
    }
}

