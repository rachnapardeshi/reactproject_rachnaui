import logo from './payment.jpg';
import './App.css';
import PaymentStatus from "./components/PaymentStatus";
import Greet from "./components/Greet";
import React from "react";
import PaymentRowCount from "./components/PaymentRowCount";

function App() {
  return (
      <div>
      <div>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <p>
            Allstate Payment Service
          </p>
        </header>
        <div className="container">
        <Greet name="Allstaters"> </Greet>
          <table className="table table-striped" valign="center">
            <tbody>
            <tr class="warning">
              <td>
                <h3>REST API Status</h3>
              </td>
              <td>
                <PaymentStatus name="AllState"> </PaymentStatus>
              </td>
            </tr>
            <tr class="warning">
              <td>
                <h3>Total Row Count Payment Collection</h3>
              </td>
              <td>
                <PaymentRowCount></PaymentRowCount>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
          </div>

      </div>
  );
}

export default App;
