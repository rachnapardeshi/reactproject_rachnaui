import React,{Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import logo from "./logo.jpg";

class Header extends Component {
    constructor() {
        super()
    }

    render() {
        return (
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="/">AllState Payment Service</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><Link to="/addPayment">Add Payment</Link></li>
                    </ul>
                    <ul className="nav navbar-nav">
                        <li className="active"><Link to="/findById">Find PaymentById</Link></li>
                    </ul>
                    <ul className="nav navbar-nav">
                        <li className="active"><Link to="/findByType">Find PaymentByType</Link></li>
                    </ul>
                    <ul className="nav navbar-nav">
                        <li className="active"><Link to="/getAllpayments">Get All Payments</Link></li>
                    </ul>
                    <ul>
                        <img src={logo} align="right" width="120px" height="60px" alt="logo"/>
                    </ul>
                </div>
            </nav>
        )
    }
}
export default withRouter(Header);