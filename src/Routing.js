import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import PaymentTable from './components/PaymentTable';
import AddPayment from "./components/AddPayment";
import App from "./App";
import FindById from "./components/FindById";
import FindByType from "./components/FindByType";

const Routing = () => {
    return(
        <BrowserRouter>
            <Header/>
            <Route exact path="/" component={App}/>
             <Route path="/getAllpayments" component={PaymentTable}/>
            <Route path="/findById" component={FindById}/>
            <Route path="/findByType" component={FindByType}/>
            <Route path="/addPayment" component={AddPayment}/>
            <Footer/>
        </BrowserRouter>
    )
}

export default Routing